from dectobin import dectobin


def test_basic_append_to_list():
    assert dectobin.append_to_list([0, 0, 0, 1], 9) == [0, 0, 0, 0, 0, 1, 0, 0, 0]


def test_basic_decimal_to_list():
    assert dectobin.decimal_to_list(1000) == (10, [0, 0, 0, 1, 0, 1, 1, 1, 1, 1])


def test_basic_build_string():
    assert dectobin.build_string([1, 0, 0, 0]) == "1000"
