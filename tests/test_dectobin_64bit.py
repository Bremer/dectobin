from dectobin import dectobin

def test_0_decimal_value():
    assert dectobin.dec_to_64bit_binary(0) == "0" * 64


def test_446744073709551615_decimal_value():
    assert dectobin.dec_to_64bit_binary(446744073709551615) == "0000011000110011001001110101111000111010111101111111111111111111"

def test_18446744073709551615_decimal_value():
    assert dectobin.dec_to_64bit_binary(18446744073709551615) == "1" * 64


def test_return_too_big_decimal_value():
    assert dectobin.dec_to_64bit_binary(18446744073709551615+1) is None


def test_return_string_none():
    assert dectobin.dec_to_64bit_binary("aaa") is None


def test_return_negative_none():
    assert dectobin.dec_to_64bit_binary(-1) is None
