from dectobin import dectobin

def test_0_decimal_value():
    assert dectobin.dec_to_8bit_binary(0) == "00000000"


def test_15_decimal_value():
    assert dectobin.dec_to_8bit_binary(15) == "00001111"


def test_255_decimal_value():
    assert dectobin.dec_to_8bit_binary(255) == "11111111"


def test_return_too_big_decimal_value():
    assert dectobin.dec_to_8bit_binary(255+1) is None


def test_return_string_none():
    assert dectobin.dec_to_8bit_binary("aaa") is None


def test_return_negative_none():
    assert dectobin.dec_to_8bit_binary(-1) is None
