from dectobin import dectobin

def test_0_decimal_value():
    assert dectobin.dec_to_32bit_binary(0) == "0" * 32


def test_65535_decimal_value():
    assert dectobin.dec_to_32bit_binary(4294967295) == "1" * 32


def test_return_too_big_decimal_value():
    assert dectobin.dec_to_32bit_binary(4294967295+1) is None


def test_return_string_none():
    assert dectobin.dec_to_32bit_binary("aaa") is None


def test_return_negative_none():
    assert dectobin.dec_to_32bit_binary(-1) is None
