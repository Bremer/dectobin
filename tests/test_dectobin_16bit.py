from dectobin import dectobin

def test_0_decimal_value():
    assert dectobin.dec_to_16bit_binary(0) == "0" * 16


def test_256_decimal_value():
    assert dectobin.dec_to_16bit_binary(256) == "0000000100000000"


def test_65535_decimal_value():
    assert dectobin.dec_to_16bit_binary(65535) == "1111111111111111"


def test_return_too_big_decimal_value():
    assert dectobin.dec_to_16bit_binary(65535+1) is None


def test_return_string_none():
    assert dectobin.dec_to_16bit_binary("aaa") is None


def test_return_negative_none():
    assert dectobin.dec_to_16bit_binary(-1) is None
