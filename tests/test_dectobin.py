from dectobin import dectobin


def test_0_decimal_value():
    assert dectobin.dectobin(0) == "00000000"

def test_1_decimal_value():
    assert dectobin.dectobin(1) == "00000001"


def test_15_decimal_value():
    assert dectobin.dectobin(15) == "00001111"


def test_32_decimal_value():
    assert dectobin.dectobin(32) == "00100000"


def test_128_decimal_value():
    assert dectobin.dectobin(128) == "10000000"


def test_256_decimal_value():
    assert dectobin.dectobin(256) == "0000000100000000"


def test_4294967295_decimal_value():
    assert dectobin.dectobin(4294967295) == "11111111111111111111111111111111"


def test_446744073709551615_decimal_value():
    assert dectobin.dectobin(446744073709551615) == "0000011000110011001001110101111000111010111101111111111111111111"


def test_18446744073709551615_decimal_value():
    assert dectobin.dectobin(18446744073709551615) == "1111111111111111111111111111111111111111111111111111111111111111"


def test_return_too_big_decimal_value():
    assert dectobin.dectobin(18446744073709551615+1) is None


def test_return_string_none():
    assert dectobin.dectobin("aaa") is None


def test_return_negative_none():
    assert dectobin.dectobin(-1) is None
