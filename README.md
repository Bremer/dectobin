Small library to convert a decimal value to a binary value.
The functions in the library will return a string representation of the binary value.
It only supports maximum 64-bit decimal value and no negative values.

```
$ pytest tests/ --cov=dectobin .
========================================== test session starts ===========================================
platform linux -- Python 3.11.4, pytest-7.4.3, pluggy-1.3.0
rootdir: /home/rickard/kod/dectobin
plugins: cov-4.1.0
collected 38 items                                                                                       

tests/test_dectobin.py ............                                                                [ 31%]
tests/test_dectobin_16bit.py ......                                                                [ 47%]
tests/test_dectobin_32bit.py .....                                                                 [ 60%]
tests/test_dectobin_64bit.py ......                                                                [ 76%]
tests/test_dectobin_8bit.py ......                                                                 [ 92%]
tests/test_helper.py ...                                                                           [100%]

---------- coverage: platform linux, python 3.11.4-final-0 -----------
Name                           Stmts   Miss  Cover
--------------------------------------------------
__init__.py                        0      0   100%
dectobin.py                       82      0   100%
tests/__init__.py                  0      0   100%
tests/test_dectobin.py            25      0   100%
tests/test_dectobin_8bit.py       13      0   100%
tests/test_dectobin_16bit.py      13      0   100%
tests/test_dectobin_32bit.py      11      0   100%
tests/test_dectobin_64bit.py      13      0   100%
tests/test_helper.py               7      0   100%
--------------------------------------------------
TOTAL                            164      0   100%


=========================================== 38 passed in 0.05s ===========================================
$ pylint dectobin.py

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

```
Use in file by:
```
import dectobin
```
And call the function with a decimal value
```
dectobin.dectobin(15)
```

Will return None if faulty input.
```
In [1]: import dectobin

In [2]: dectobin.dectobin(256)
Out[2]: '0000000100000000'

In [3]: dectobin.dectobin("aa")

```
