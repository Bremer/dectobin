""" Small library to convert a decimal value to an binary value """


def dectobin(decimal_number):
    """ This function does the actual work that converts the decimal
    value to an binary value and returns it as a string. Will return
    None if input is not an integer or to high number."""
    if isinstance(decimal_number, int):
        if decimal_number >= 0:
            list_length, binary_list = decimal_to_list(decimal_number)
            if list_length < 9:
                list_size = 8
            elif list_length < 17:
                list_size = 16
            elif list_length < 33:
                list_size = 32
            elif list_length < 65:
                list_size = 64
            else:
                return None
            binary_list_reverse = append_to_list(binary_list, list_size)
        elif decimal_number < 0:
            return None
    else:
        return None
    return build_string(binary_list_reverse)


def dec_to_8bit_binary(decimal_number):
    """ Decimal to 8bit conversion """
    if isinstance(decimal_number, int):
        if decimal_number >= 0:
            list_length, binary_list = decimal_to_list(decimal_number)
            if list_length < 9:
                list_size = 8
            else:
                return None
            binary_list_reverse = append_to_list(binary_list, list_size)
        elif decimal_number < 0:
            return None
    else:
        return None
    return build_string(binary_list_reverse)


def dec_to_16bit_binary(decimal_number):
    """ Decimal to 16bit conversion """
    if isinstance(decimal_number, int):
        if decimal_number >= 0:
            list_length, binary_list = decimal_to_list(decimal_number)
            if list_length < 17:
                list_size = 16
            else:
                return None
        elif decimal_number < 0:
            return None
        binary_list_reverse = append_to_list(binary_list, list_size)
    else:
        return None
    return build_string(binary_list_reverse)


def dec_to_32bit_binary(decimal_number):
    """ Decimal to 32bit conversion """
    if isinstance(decimal_number, int):
        if decimal_number >= 0:
            list_length, binary_list = decimal_to_list(decimal_number)
            if list_length < 33:
                list_size = 32
            else:
                return None
            binary_list_reverse = append_to_list(binary_list, list_size)
        elif decimal_number < 0:
            return None
    else:
        return None
    return build_string(binary_list_reverse)


def dec_to_64bit_binary(decimal_number):
    """ Decimal to 64bit conversion """
    if isinstance(decimal_number, int):
        if decimal_number >= 0:
            list_length, binary_list = decimal_to_list(decimal_number)
            if list_length < 65:
                list_size = 64
            else:
                return None
            binary_list_reverse = append_to_list(binary_list, list_size)
        elif decimal_number < 0:
            return None
    else:
        return None
    return build_string(binary_list_reverse)


def build_string(binary_list):
    """ Build string """
    string_representation_of_binary_value = str()
    for item in binary_list:
        string_representation_of_binary_value += str(item)
    return string_representation_of_binary_value


def decimal_to_list(decimal_number):
    """ Build list from decimal """
    binary_list = []
    while decimal_number:
        binary_list.append(decimal_number % 2)
        decimal_number = int(decimal_number // 2)
    return len(binary_list), binary_list


def append_to_list(binary_list, list_size):
    """ Append to list and reverse it """
    while len(binary_list) < list_size:
        binary_list.append(0)
    binary_list.reverse()
    return binary_list
